#!make -f

#CXX=clang++-9 
CXX=gcc
CXXFLAGS= -Wall -g

SRC=src
BIN=bin

HEADERS=$(SRC)/AdptArray.h $(SRC)/book.h $(SRC)/Person.h
SOURCES=$(SRC)/AdptArray.c $(SRC)/book.c $(SRC)/Person.c
OBJECTS=$(BIN)/AdptArray.o $(BIN)/book.o $(BIN)/Person.o

# CI/CD
clean:
	rm -rf *.o bin demo

# units
bin:
	mkdir bin

$(BIN)/%.o: $(SRC)/%.c bin $(HEADERS)
	$(CXX) $(CXXFLAGS) --compile $< -o $@

# start
demo: $(BIN)/Demo.o $(OBJECTS)
	$(CXX) $(CXXFLAGS) $^ -o demo

run: demo
	./$^

# testing
mem_test: demo
	valgrind ./$^
