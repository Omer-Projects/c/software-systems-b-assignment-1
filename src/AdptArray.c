#include "AdptArray.h"

#include <stdlib.h>
#include <stdio.h>
#include <strings.h>

struct AdptArray_
{
    size_t length;
    PElement *data;
    COPY_FUNC copy_element;
    DEL_FUNC delete_element;
    PRINT_FUNC print_element;
};

PAdptArray CreateAdptArray(COPY_FUNC copy_element, DEL_FUNC delete_element, PRINT_FUNC print_element)
{
    PAdptArray adptArray = malloc(sizeof(struct AdptArray_));

    if (adptArray == NULL)
    {
        return NULL;
    }

    adptArray->length = 0;
    adptArray->data = NULL;
    adptArray->copy_element = copy_element;
    adptArray->delete_element = delete_element;
    adptArray->print_element = print_element;

    return adptArray;
}

void DeleteAdptArray(PAdptArray self)
{
    if (self == NULL)
    {
        return;
    }

    PElement *data = self->data;
    DEL_FUNC delete_element = self->delete_element;
    if (self->length > 0 && data != NULL)
    {
        PElement *ptr = data + self->length;
        while (ptr != data)
        {
            ptr -= 1;
            if (*ptr != NULL)
            {
                (*delete_element)(*ptr);
            }
        }
    }

    free(data);
    free(self);
}

Result SetAdptArrayAt(PAdptArray self, int index, PElement element)
{
    if (index < 0 && self == NULL)
    {
        return FAIL;
    }

    if (index < self->length && self->data[index] != NULL)
    {
        (*self->delete_element)(self->data[index]);
    }

    if (index >= self->length)
    {
        PElement *data = (PElement *)calloc(sizeof(PElement) * (index + 1), 1);
        PElement *oldData = self->data;
        if (data == NULL)
        {
            return FAIL;
        }

        for (size_t i = 0; i < self->length; i++)
        {
            if (oldData[i] == NULL)
            {
                data[i] = NULL;
            }
            else
            {
                data[i] = (*self->copy_element)(oldData[i]);
                (*self->delete_element)(oldData[i]);
            }
        }

        free(oldData);

        self->data = data;
        self->length = index + 1;
    }

    if (element == NULL)
    {
        self->data[index] = NULL;
    }
    else
    {
        self->data[index] = self->copy_element(element);
    }

    return SUCCESS;
}

PElement GetAdptArrayAt(PAdptArray self, int index)
{
    if (self == NULL)
    {
        return NULL;
    }

    if (index < 0 || index >= self->length)
    {
        return NULL;
    }

    PElement element = self->data[index];
    if (element != NULL)
    {
        element = (*self->copy_element)(element);
    }

    return element;
}

int GetAdptArraySize(PAdptArray self)
{
    if (self == NULL)
    {
        return -1;
    }

    return self->length;
}

void PrintDB(PAdptArray self)
{
    if (self == NULL)
    {
        printf("null\n");
        return;
    }

    printf("[ ");

    for (size_t i = 0; i < self->length; i++)
    {
        if (i > 0)
        {
            printf(" , ");
        }

        if (self->data[i] == NULL)
        {
            printf("null");
        }
        else
        {
            (*(self->print_element))(self->data[i]);
        }
    }

    printf(" ]\n");
}
